package com.ofovdevelopment.bungeedefaultserver;


import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class BungeeDefaultServer extends Plugin {


    public static BungeeDefaultServer plugin;
    private Configuration configuration;

    @Override
    public void onEnable() {
        plugin = this;
        createConfiguration();
        System.out.println("Plugin by OFOV development team.");
        System.out.println("Default Server: (" + getConfiguration().getString("defaultServer") + ")");
    }

    @Override
    public void onDisable() {
        plugin = null;
    }

    public Configuration getConfiguration() {
        return configuration;
    }


    private void createConfiguration() {
        File folder = new File(getDataFolder(), "");
        if (!folder.exists()) {
            folder.mkdir();
        }
        File file = new File(getDataFolder() + "/config.yml");
        if (!file.exists()) {
            try {
                String path = "defaultServer: Hub";
                FileWriter fileWriter = new FileWriter(file);
                BufferedWriter out = new BufferedWriter(fileWriter);
                out.write(path);
                out.close();
                fileWriter.close();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
        ConfigurationProvider configurationProvider = ConfigurationProvider.getProvider(YamlConfiguration.class);
        try {
            configuration = configurationProvider.load(file);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
