package com.ofovdevelopment.bungeedefaultserver.listeners;

import com.ofovdevelopment.bungeedefaultserver.BungeeDefaultServer;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;


public class ServerKickListener implements Listener {

    @EventHandler
    public void kickPlayer(ServerKickEvent event) {
        ServerInfo server = ProxyServer.getInstance().getServerInfo(BungeeDefaultServer.plugin.getConfiguration().getString("defaultServer"));
        event.getPlayer().connect(server);
        event.setCancelled(true);
        event.getPlayer().sendMessage(ChatColor.RED + "You were sent to the default server: " + ChatColor.RESET + event.getKickReason());
    }
}
